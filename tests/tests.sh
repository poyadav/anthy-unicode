#!/bin/sh

: ${SRCDIR:='./source'}

anthy-dic-tool-unicode --load $SRCDIR/src-util/dic-tool-input
diff $HOME/.config/anthy/private_words_default $SRCDIR/src-util/dic-tool-result
anthy-dic-tool-unicode --dump
[ -d $HOME/.anthy ] || mkdir $HOME/.anthy
mv $HOME/.config/anthy/private_words_default $HOME/.anthy
anthy-dic-tool-unicode --migrate
diff $HOME/.config/anthy/private_words_default $SRCDIR/src-util/dic-tool-result

